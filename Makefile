#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/03 00:06:30 by msabwat           #+#    #+#              #
#    Updated: 2021/03/28 03:04:23 by msabwat          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

EXE1=checker

EXE2=push_swap

FLAGS=-Wall -Wextra -Werror

CC=clang

INC=-I.

LIBFT_INC=-Ilibft/

SRC_NAME= 	check_options.c \
			ops_swap.c \
			ops_push.c \
			ops_rotate.c \
			ops_reverse.c \
			utils.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

TEST=

all: makedir $(EXE1) $(EXE2)

makedir:
	@mkdir -p .obj

test: FLAGS += -g -fsanitize=address
test: TEST = test
test: makedir $(EXE1) $(EXE2)

$(EXE1): $(OBJS)
	$(MAKE) $(TEST) -C libft
	$(CC) $(FLAGS) checker.c $(OBJS) $(LIBFT_INC) $(INC) -L libft/ -lft -o $(EXE1)

$(EXE2): $(OBJS)
	$(MAKE) $(TEST) -C libft
	$(CC) $(FLAGS) push_swap.c $(SRC_PATH)/push_swap_utils.c \
								$(SRC_PATH)/partition.c \
								$(SRC_PATH)/big_partition.c \
								$(SRC_PATH)/sort.c \
								$(SRC_PATH)/sort_utils.c \
	$(OBJS) $(LIBFT_INC) $(INC) -L libft/ -lft -o $(EXE2)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(EXE1) $(EXE2)

fclean: clean
	rm -fr $(OBJ_PATH)
	$(MAKE) fclean -C libft

re: fclean all
