/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:27:24 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 03:41:22 by msabwat          ###   ########.fr       */
/*   Updated: 2021/03/28 15:49:45 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft/libft.h"

# define PART_500 11

# define PART_100 4

typedef enum	e_op
{
	INVALID,
	SWAPA,
	SWAPB,
	SWAPAB,
	PUSHA,
	PUSHB,
	ROTATEA,
	ROTATEB,
	ROTATEAB,
	REVERSEA,
	REVERSEB,
	REVERSEAB,
}				t_op;

typedef struct	s_sys
{
	int			size;
	t_stack		*stack_a;
	t_stack		*stack_b;
	int			*arr;
	int			median;
	int			max;
	int			min;
}				t_sys;

long long		ft_atoll(const char *str);
int				check_options(int ac, char **av);
void			init_sys(t_sys **sys, int ac, char **av);
void			free_sys(t_sys *sys);

void			set_min_max(t_sys *sys, int *min, int *max);

void			ops_swap(t_stack *stack);
void			ops_swap2(t_stack *stack1, t_stack *stack2);
void			ops_push(t_stack *src, t_stack *dst);
void			ops_rotate(t_stack *stack);
void			ops_rotate2(t_stack *stack1, t_stack *stack2);
void			ops_reverse(t_stack *stack);
void			ops_reverse2(t_stack *stack1, t_stack *stack2);

void			apply_instruction(t_sys *sys, t_op op);
int				is_ordered(t_stack *stack);

void			sort_partition(t_sys *sys, int begin, int end);
void			sort_b_partition(t_sys *sys, int begin, int end);
void			prepare_partition(t_sys *sys, int begin, int end, int div);
void			position_stack_a(t_sys *sys);
void			sort_small(t_sys *sys);
void			sort_smoll(t_sys *sys);
void			sort_medium(t_sys *sys);
void			sort_big(t_sys *sys);
void			handle_small_partition(t_sys *sys);
void			handle_sort_3(t_sys *sys, int last, int first);
void			handle_sort_4(t_sys *sys);
void			handle_sort_5(t_sys *sys);
#endif
