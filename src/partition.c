/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   partition.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 02:26:26 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 18:12:07 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		position_stackb(t_sys *sys, int value, int index)
{
	if (index > (int)sys->stack_b->r_size / 2)
	{
		while (ft_stackpeek(sys->stack_b) != value)
		{
			ft_putendl_fd("rb", 1);
			apply_instruction(sys, ROTATEB);
		}
	}
	else
	{
		while (ft_stackpeek(sys->stack_b) != value)
		{
			ft_putendl_fd("rrb", 1);
			apply_instruction(sys, REVERSEB);
		}
	}
}

static int		choose_index(t_sys *sys, int *begin, int *end)
{
	int i1;
	int i2;

	i1 = 0;
	i2 = 0;
	while (i1 < (int)sys->stack_b->r_size)
	{
		if (sys->stack_b->items[i1] == sys->arr[*begin])
			break ;
		i1++;
	}
	while (i2 < (int)sys->stack_b->r_size)
	{
		if (sys->stack_b->items[i2] == sys->arr[*end])
			break ;
		i2++;
	}
	if (i1 > i2)
		*end -= 1;
	else
		*begin += 1;
	return ((i1 > i2) ? i2 : i1);
}

static void		fetch_and_push(t_sys *sys, int *begin, int *end)
{
	int index;
	int val1;
	int value;

	val1 = sys->arr[*begin];
	index = choose_index(sys, begin, end);
	value = sys->stack_b->items[index];
	position_stackb(sys, value, index);
	if (ft_stackpeek(sys->stack_b) == val1)
	{
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
	}
	else
	{
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
	}
}

void			sort_partition(t_sys *sys, int begin, int end)
{
	int b_index;
	int e_index;
	int index;
	int size;

	b_index = begin;
	e_index = end;
	index = 0;
	size = (int)sys->stack_b->r_size;
	while (index < size)
	{
		fetch_and_push(sys, &b_index, &e_index);
		index++;
	}
}

void			prepare_partition(t_sys *sys, int begin, int end, int size)
{
	while ((int)sys->stack_b->r_size <= size)
	{
		if ((ft_stackpeek(sys->stack_a) <= end)
				&& (ft_stackpeek(sys->stack_a) >= begin))
		{
			ft_putendl_fd("pb", 1);
			apply_instruction(sys, PUSHB);
		}
		ft_putendl_fd("rra", 1);
		apply_instruction(sys, REVERSEA);
	}
}
