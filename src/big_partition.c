/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   partition.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 02:26:26 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 15:28:15 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		find_index(t_sys *sys, int value)
{
	int i;

	i = 0;
	while (i < (int)sys->stack_b->r_size)
	{
		if ((sys->stack_b->items[i] == value) && (sys->stack_b->flags[i] == 1))
			break ;
		i++;
	}
	return (i);
}

static void		fetch_value(t_sys *sys, int value)
{
	int index;

	index = find_index(sys, value);
	if (index >= (int)(sys->stack_b->r_size / 2))
	{
		while (ft_stackpeek(sys->stack_b) != value)
		{
			ft_putendl_fd("rb", 1);
			apply_instruction(sys, ROTATEB);
		}
	}
	else
	{
		while (ft_stackpeek(sys->stack_b) != value)
		{
			ft_putendl_fd("rrb", 1);
			apply_instruction(sys, REVERSEB);
		}
	}
}

void			sort_b_partition(t_sys *sys, int up_limit, int down_limit)
{
	int i_val;

	i_val = down_limit;
	while (i_val != up_limit)
	{
		fetch_value(sys, sys->arr[i_val]);
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
		i_val--;
	}
	fetch_value(sys, sys->arr[i_val]);
	ft_putendl_fd("pa", 1);
	apply_instruction(sys, PUSHA);
}
