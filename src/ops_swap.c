/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ops_swap.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:36:10 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/24 18:10:51 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ops_swap(t_stack *stack)
{
	int	tmp;
	int size;

	size = stack->r_size;
	if (stack->top == -1 || stack->top == 0)
		return ;
	tmp = stack->items[size - 1];
	stack->items[size - 1] = stack->items[size - 2];
	stack->items[size - 2] = tmp;
}

void		ops_swap2(t_stack *stack1, t_stack *stack2)
{
	if (stack1->top != -1 && stack1->top != 0)
		ops_swap(stack1);
	if (stack2->top != -1 && stack2->top != 0)
		ops_swap(stack2);
}
