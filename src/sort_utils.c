/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 02:26:45 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/24 02:23:13 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			handle_small_partition(t_sys *sys)
{
	int i;

	i = 0;
	while (i < sys->size)
	{
		if (((ft_stackpeek(sys->stack_a) == sys->min)
				|| (ft_stackpeek(sys->stack_a) == sys->max)) && sys->size == 4)
		{
			ft_putendl_fd("pb", 1);
			apply_instruction(sys, PUSHB);
			break ;
		}
		if (((ft_stackpeek(sys->stack_a) == sys->min)
				|| (ft_stackpeek(sys->stack_a) == sys->max)) && sys->size == 5)
		{
			ft_putendl_fd("pb", 1);
			apply_instruction(sys, PUSHB);
		}
		i++;
		if ((i >= 2) && (sys->stack_b->r_size == 2))
			break ;
		ft_putendl_fd("rra", 1);
		apply_instruction(sys, REVERSEA);
	}
}

static void		handle_reverse(t_sys *sys, int flag)
{
	ft_putendl_fd("sa", 1);
	apply_instruction(sys, SWAPA);
	if (flag == 1)
	{
		ft_putendl_fd("rra", 1);
		apply_instruction(sys, REVERSEA);
	}
	else
	{
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
	}
}

void			handle_sort_3(t_sys *sys, int last, int first)
{
	int min;
	int max;

	min = 0;
	max = 0;
	set_min_max(sys, &min, &max);
	if (last == max && first == min)
		handle_reverse(sys, 1);
	else if (last == max && first != min)
	{
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
	}
	else if (first == max && last != min)
	{
		ft_putendl_fd("sa", 1);
		apply_instruction(sys, SWAPA);
	}
	else if (last == min && first != max)
		handle_reverse(sys, 0);
	else if (first == min && last != max)
	{
		ft_putendl_fd("rra", 1);
		apply_instruction(sys, REVERSEA);
	}
}

void			handle_sort_4(t_sys *sys)
{
	if (ft_stackpeek(sys->stack_b) == sys->max)
	{
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
	}
	else
	{
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
	}
}

void			handle_sort_5(t_sys *sys)
{
	if (ft_stackpeek(sys->stack_b) == sys->max)
	{
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
	}
	else
	{
		ft_putendl_fd("rb", 1);
		apply_instruction(sys, ROTATEA);
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
		ft_putendl_fd("pa", 1);
		apply_instruction(sys, PUSHA);
	}
}
