/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ops_reverse.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:35:52 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/14 03:36:49 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ops_reverse(t_stack *stack)
{
	int	size;
	int	i;
	int	j;
	int arr[stack->r_size];

	size = stack->r_size;
	i = 0;
	j = 1;
	if (stack->top == -1)
		return ;
	i = 0;
	while (i < size)
	{
		arr[i] = stack->items[(j + i) % size];
		i++;
	}
	i = 0;
	while (i < size)
	{
		stack->items[i] = arr[i];
		i++;
	}
}

void		ops_reverse2(t_stack *stack1, t_stack *stack2)
{
	ops_reverse(stack1);
	ops_reverse(stack2);
}
