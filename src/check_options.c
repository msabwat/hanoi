/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_options.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:36:28 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 15:39:29 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int		check_option_isnum(char *av)
{
	int j;

	j = 0;
	while (j < (int)ft_strlen(av))
	{
		if ((!ft_isdigit(av[j])) && (av[j] != '-'))
			return (0);
		j++;
	}
	return (1);
}

static int		check_duplicate(int ac, char **av)
{
	int			i;
	int			j;
	long long	tmp;

	i = 1;
	j = 0;
	while (i < ac)
	{
		tmp = ft_atoll(av[i]);
		j = i + 1;
		while (j < ac)
		{
			if (ft_atoll(av[j]) == tmp)
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}

int				check_options(int ac, char **av)
{
	int	i;

	i = 1;
	while (i < ac)
	{
		if (ft_strlen(av[i]) > 11)
			return (0);
		if (!(check_option_isnum(av[i]))
			|| (ft_atoll(av[i]) > 2147483647)
			|| (ft_atoll(av[i]) < -2147483648))
			return (0);
		i++;
	}
	if (!check_duplicate(ac, av))
		return (0);
	return (1);
}
