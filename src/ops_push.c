/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ops_push.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:35:10 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/14 02:06:54 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		ops_push(t_stack *src, t_stack *dst)
{
	if (src->top != -1)
		ft_stackpush(dst, ft_stackpop(src));
}
