/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 02:24:43 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/24 01:57:00 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			apply_instruction(t_sys *sys, t_op op)
{
	if (op == SWAPA)
		ops_swap(sys->stack_a);
	else if (op == SWAPB)
		ops_swap(sys->stack_b);
	else if (op == SWAPAB)
		ops_swap2(sys->stack_a, sys->stack_b);
	else if (op == PUSHA)
		ops_push(sys->stack_b, sys->stack_a);
	else if (op == PUSHB)
		ops_push(sys->stack_a, sys->stack_b);
	else if (op == ROTATEA)
		ops_rotate(sys->stack_a);
	else if (op == ROTATEB)
		ops_rotate(sys->stack_b);
	else if (op == ROTATEAB)
		ops_rotate2(sys->stack_a, sys->stack_b);
	else if (op == REVERSEA)
		ops_reverse(sys->stack_a);
	else if (op == REVERSEB)
		ops_reverse(sys->stack_b);
	else if (op == REVERSEAB)
		ops_reverse2(sys->stack_a, sys->stack_b);
}

int				is_ordered(t_stack *stack)
{
	int i;

	i = 0;
	if (stack->r_size != stack->size)
		return (0);
	while (i < (int)stack->size - 1)
	{
		if (stack->items[i] < stack->items[i + 1])
			return (0);
		i++;
	}
	return (1);
}

void			set_min_max(t_sys *sys, int *min, int *max)
{
	int		i;
	int		tmp_min;
	int		tmp_max;
	t_stack	*stack;

	i = 0;
	tmp_min = sys->max;
	tmp_max = sys->min;
	stack = sys->stack_a;
	while (i < (int)stack->r_size)
	{
		if ((stack->items[i] > tmp_max) && (stack->items[i] <= sys->max)
			&& (stack->flags[i] == 1))
			tmp_max = stack->items[i];
		if ((stack->items[i] < tmp_min) && (stack->items[i] >= sys->min)
			&& (stack->flags[i] == 1))
			tmp_min = stack->items[i];
		i++;
	}
	*min = tmp_min;
	*max = tmp_max;
}

void			position_stack_a(t_sys *sys)
{
	while (ft_stackpeek(sys->stack_a) != sys->min)
	{
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
	}
}
