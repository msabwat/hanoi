/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 02:25:09 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 16:02:49 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			sort_small(t_sys *sys)
{
	if (sys->size == 2)
	{
		ft_putendl_fd("sa", 1);
		apply_instruction(sys, SWAPA);
		return ;
	}
	else if (sys->size == 3)
	{
		handle_sort_3(sys, sys->stack_a->items[2], sys->stack_a->items[0]);
		return ;
	}
	else if (sys->size == 4)
	{
		handle_small_partition(sys);
		handle_sort_3(sys, sys->stack_a->items[2], sys->stack_a->items[0]);
		handle_sort_4(sys);
	}
	else if (sys->size == 5)
	{
		handle_small_partition(sys);
		handle_sort_3(sys, sys->stack_a->items[2], sys->stack_a->items[0]);
		handle_sort_5(sys);
	}
}

static int		*setup_part(t_sys *sys, int number)
{
	int		*part;
	int		index;
	int		val;

	part = (int *)malloc(sizeof(int) * number);
	if (!part)
		return (NULL);
	val = 0;
	index = 0;
	if (sys->size % number)
		val = 0;
	else
		val = 1;
	while (index < number - 1)
	{
		part[index] = ((sys->size / number) * (index + 1)) - val;
		index++;
	}
	part[index] = sys->size - 1;
	return (part);
}

void			sort_smoll(t_sys *sys)
{
	int median_index;

	if (sys->size % 2 == 0)
		median_index = (sys->size / 2) - 1;
	else
		median_index = (sys->size / 2);
	prepare_partition(sys, sys->min, sys->arr[median_index], median_index);
	sort_partition(sys, 0, median_index);
	prepare_partition(sys, sys->arr[median_index],
							sys->max, sys->size - median_index - 1);
	ft_putendl_fd("ra", 1);
	apply_instruction(sys, ROTATEA);
	sort_partition(sys, median_index, sys->size - 1);
	position_stack_a(sys);
}

void			sort_medium(t_sys *sys)
{
	int *part;
	int i;
	int size;
	int val;

	i = 0;
	size = 0;
	val = 0;
	if (!(part = setup_part(sys, PART_100)))
		return ;
	prepare_partition(sys, sys->min, sys->arr[part[0]], part[0]);
	sort_partition(sys, 0, part[0]);
	while (i < PART_100 - 1)
	{
		size = (i == PART_100 - 2) ? part[i + 1] - part[i] - 1
									: part[i + 1] - part[i];
		val = (i == PART_100 - 2) ? sys->max : sys->arr[part[i + 1]];
		prepare_partition(sys, sys->arr[part[i]], val, size);
		ft_putendl_fd("ra", 1);
		apply_instruction(sys, ROTATEA);
		sort_partition(sys, part[i], part[i + 1]);
		i++;
	}
	position_stack_a(sys);
	free(part);
}

void			sort_big(t_sys *sys)
{
	int *part;
	int i;

	i = 0;
	if (!(part = setup_part(sys, PART_500)))
		return ;
	prepare_partition(sys, sys->min, sys->arr[part[i]], part[i]);
	while (i < PART_500 - 2)
	{
		prepare_partition(sys, sys->arr[part[i] + 1],
								sys->arr[part[i + 1]], part[i + 1]);
		i++;
	}
	prepare_partition(sys, sys->arr[part[i] + 1], sys->max, sys->size - 1);
	i = PART_500 - 2;
	sort_b_partition(sys, part[i] + 1, sys->size - 1);
	while (i != 0)
	{
		sort_b_partition(sys, part[i - 1] + 1, part[i]);
		i--;
	}
	sort_b_partition(sys, 0, part[i]);
	free(part);
}
