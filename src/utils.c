/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:36:39 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 18:11:02 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <push_swap.h>

static int		toskip(char c)
{
	if ((c <= 32 && c != 27 && c != '\200') && c != '\0')
		return (1);
	return (0);
}

long long		ft_atoll(const char *str)
{
	int			cnt;
	long long	number;
	int			s;

	s = 0;
	number = 0;
	cnt = 0;
	while (toskip(str[cnt]))
		cnt++;
	if (str[cnt] == '+' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9')
		cnt++;
	if (str[cnt] == '-' && str[cnt + 1] >= '0' && str[cnt + 1] <= '9' && s == 0)
	{
		cnt++;
		s = 1;
	}
	while ((str[cnt] >= '0' && str[cnt] <= '9') && str[cnt] != '\0')
	{
		if (s == 0)
			number = 10 * number + str[cnt] - 48;
		else
			number = 10 * number - str[cnt] + 48;
		cnt++;
	}
	return (number);
}

void			init_sys(t_sys **sys, int ac, char **av)
{
	int		i;

	i = ac - 1;
	*sys = (t_sys *)malloc(sizeof(t_sys));
	if (!(*sys))
		return ;
	(*sys)->size = i;
	(*sys)->stack_a = NULL;
	(*sys)->stack_b = NULL;
	(*sys)->stack_a = ft_stacknew(i);
	if (!((*sys)->stack_a))
		return ;
	(*sys)->stack_b = ft_stacknew(i);
	if (!((*sys)->stack_b))
		return ;
	while (i != 0)
		ft_stackpush((*sys)->stack_a, ft_atoi(av[i--]));
	(*sys)->median = 0;
	(*sys)->max = 0;
	(*sys)->min = 0;
	(*sys)->arr = NULL;
}

void			free_sys(t_sys *sys)
{
	ft_stackdel(sys->stack_a);
	ft_stackdel(sys->stack_b);
	if (sys->arr)
		free(sys->arr);
	free(sys);
}
