/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/11 02:27:13 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 15:39:21 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include <stdlib.h>

void	validate_instruction(char *ins, t_op *instruction)
{
	if (!ft_strcmp(ins, "sa"))
		*instruction = SWAPA;
	else if (!ft_strcmp(ins, "sb"))
		*instruction = SWAPB;
	else if (!ft_strcmp(ins, "ss"))
		*instruction = SWAPAB;
	else if (!ft_strcmp(ins, "pa"))
		*instruction = PUSHA;
	else if (!ft_strcmp(ins, "pb"))
		*instruction = PUSHB;
	else if (!ft_strcmp(ins, "ra"))
		*instruction = ROTATEA;
	else if (!ft_strcmp(ins, "rb"))
		*instruction = ROTATEB;
	else if (!ft_strcmp(ins, "rr"))
		*instruction = ROTATEAB;
	else if (!ft_strcmp(ins, "rra"))
		*instruction = REVERSEA;
	else if (!ft_strcmp(ins, "rrb"))
		*instruction = REVERSEB;
	else if (!ft_strcmp(ins, "rrr"))
		*instruction = REVERSEAB;
	else
		ft_putstr_fd("Error\n", 2);
	ft_strdel(&ins);
}

void	apply_instruction(t_sys *sys, t_op op)
{
	if (op == SWAPA)
		ops_swap(sys->stack_a);
	else if (op == SWAPB)
		ops_swap(sys->stack_b);
	else if (op == SWAPAB)
		ops_swap2(sys->stack_a, sys->stack_b);
	else if (op == PUSHA)
		ops_push(sys->stack_b, sys->stack_a);
	else if (op == PUSHB)
		ops_push(sys->stack_a, sys->stack_b);
	else if (op == ROTATEA)
		ops_rotate(sys->stack_a);
	else if (op == ROTATEB)
		ops_rotate(sys->stack_b);
	else if (op == ROTATEAB)
		ops_rotate2(sys->stack_a, sys->stack_b);
	else if (op == REVERSEA)
		ops_reverse(sys->stack_a);
	else if (op == REVERSEB)
		ops_reverse(sys->stack_b);
	else if (op == REVERSEAB)
		ops_reverse2(sys->stack_a, sys->stack_b);
}

int		is_stack_sorted(t_sys *sys)
{
	int i;

	i = 0;
	if (sys->stack_a->r_size != sys->stack_a->size)
	{
		ft_putstr_fd("KO\n", 1);
		free_sys(sys);
		return (0);
	}
	while (i < (int)(sys->stack_a->size - 1))
	{
		if (sys->stack_a->items[i] < sys->stack_a->items[i + 1])
		{
			ft_putstr_fd("KO\n", 1);
			free_sys(sys);
			return (0);
		}
		i++;
	}
	return (1);
}

int		is_sorted(t_sys *sys)
{
	if (sys)
	{
		if (sys->stack_b)
		{
			if (sys->stack_b->top != -1)
			{
				ft_putstr_fd("KO\n", 1);
				free_sys(sys);
				return (1);
			}
		}
		if (sys->stack_a)
		{
			if (!is_stack_sorted(sys))
				return (1);
		}
		ft_putstr_fd("OK\n", 1);
		free_sys(sys);
	}
	return (0);
}

int		main(int ac, char **av)
{
	char	*str;
	t_op	op;
	int		res;
	t_sys	*sys;

	sys = NULL;
	op = INVALID;
	if (ac == 1)
		return (0);
	if (!check_options(ac, av))
	{
		ft_putstr_fd("Error\n", 2);
		return (1);
	}
	init_sys(&sys, ac, av);
	while (42 && sys)
	{
		op = INVALID;
		res = get_next_line(0, &str);
		if (res == 0)
			break ;
		validate_instruction(str, &op);
		apply_instruction(sys, op);
	}
	return (is_sorted(sys));
}
