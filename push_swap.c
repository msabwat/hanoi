/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/03/22 02:24:02 by msabwat           #+#    #+#             */
/*   Updated: 2021/03/28 18:13:33 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void		handle_median(t_sys *sys, int *arr)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (i < sys->size - 1)
	{
		j = i + 1;
		while (j < sys->size)
		{
			if (arr[j] < arr[i])
				ft_swap(&arr[j], &arr[i]);
			j++;
		}
		i++;
	}
	if (sys->size % 2 == 0)
		sys->median = (arr[sys->size / 2] + arr[sys->size / 2 - 1]) / 2;
	else
		sys->median = arr[sys->size / 2];
	sys->max = arr[sys->size - 1];
	sys->min = arr[0];
}

static int		set_median(t_sys *sys, int *items)
{
	int		*arr;
	int		i;

	i = 0;
	if (sys->stack_a->r_size == 0)
		return (0);
	arr = malloc(sizeof(int) * (sys->size));
	if (!arr)
		return (0);
	while (i < sys->size)
	{
		arr[i] = items[i];
		i++;
	}
	handle_median(sys, arr);
	sys->arr = arr;
	return (1);
}

void			launch_sort(t_sys *sys)
{
	void	(*sort)(t_sys *);

	sort = NULL;
	if (sys->size <= 5)
		sort = &sort_small;
	else if (sys->size <= 80 && sys->size > 5)
		sort = &sort_smoll;
	else if (sys->size <= 250 && sys->size > 80)
		sort = &sort_medium;
	else
		sort = &sort_big;
	sort(sys);
}

int				main(int ac, char **av)
{
	t_sys		*sys;

	sys = NULL;
	if (!check_options(ac, av))
	{
		ft_putstr_fd("Error\n", 1);
		return (1);
	}
	init_sys(&sys, ac, av);
	if (!sys)
		return (0);
	if (is_ordered(sys->stack_a) || !set_median(sys, sys->stack_a->items))
	{
		free_sys(sys);
		return (0);
	}
	launch_sort(sys);
	free_sys(sys);
	return (0);
}
